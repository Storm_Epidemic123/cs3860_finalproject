# CS 3860 Final Project
# Joshua Rist
# Parse data for the Video_Actors and Video_Recordings collections (from respective .txt files) and put it in MongoDB

require('mongo')
require('bson')

# run this program (LAPTOP):
# irb load 'C:\Users\ristj\Desktop\CSDatabases\cs3860_finalproject\mongoDBSetUp.rb'
# run this program (DESKTOP):
# irb load 'C:\Users\Admin\Desktop\Git Repos\CS3860\cs3860_finalproject\mongoDBSetUp.rb'

client = Mongo::Client.new('mongodb+srv://ristj:acpo0957@cluster0.qhsfq.mongodb.net/VideoDB?retryWrites=true&w=majority')
Mongo::Logger.logger.level = ::Logger::FATAL

actors = client[:Video_Actors]
videoRecordings = client[:Video_Recordings]




# Read the Video_Actors.txt file, and put Actor data into a usable array in the format: [[ID, Name, [movie1, movie2, ...]]]
puts "Parsing Video_Actors.txt..."
actorNames = [] # An array that holds all of the names of the actors...
actorDataArray = [] # Initalize a blank array to hold our actor info

File.foreach('C:\Users\Admin\Desktop\Git Repos\CS3860\cs3860_finalproject\Video_Actors.txt'){|line|
		if(line != '')
			# Get the name of the actor
			lineArray = line.split('"') # Split the line by quotes. This is to account for people with 1 or more names (first, middle, last)
			movies = []
			actorData = []
			if(!(actorNames.include? lineArray[1])) # If the actor doesn't exist in the array, add it
				actorNames << lineArray[1]
				actorData << lineArray[0].split(".")[0] # Remove the decimal from the ID...
				actorData << lineArray[1]
				movies << lineArray[2].strip  # Remove the whitespace from the Recording_ID...
				actorData << movies
				actorDataArray << actorData # Add the actor data to the main ActorData array
			else # If the actor DOES exist, add an additonal movie to that actor's data
				actorDataArray.find{|actor| # actor[1] = name of the actor
					if(actor[1] == lineArray[1])
						actor[2] << lineArray[2].strip # Add the additional movie to the actor's data, strip all white space from the Recording_ID...
						end
				} 
				end
			end
			# Setup our main actorDataArray
}





# Read the Video_Recordings.txt file, and put Recording data into a usable array in the format:  
# [Recording_ID, Director, Title, [Actor_ID1, Actor_ID2, ...], Category, img_name, Duration, Release Year, Rating, Stock]
puts "Parsing Video_Recordings.txt..."
videoDataArray = [] #Blank array, will be filled with our recording data...
File.foreach('C:\Users\Admin\Desktop\Git Repos\CS3860\cs3860_finalproject\Video_Recordings.txt'){|line|
	lineArray = line.split('"') #Split the line by quotes first
	
	endData = lineArray[10].split # Split the last three values, these are NOT seperated by quotes and some extra work is required...
	lineArray[8] = lineArray[8].strip # Remove whitespace from the duration
	lineArray[10] = endData[0].strip # Set the release year
	lineArray << endData[1].strip # Set Price
	lineArray << endData[2].strip # Set Stock Count
	
	actors = []
	actorDataArray.each{|actor| # Search through our Actor data to determine which actors appear in the current Recording...
	if(actor[2].include? lineArray[0].strip)
		actorIDAndName = []
		actorIDAndName << actor[0]
		actorIDAndName << actor[1]
		actors << actorIDAndName
		end
	}
	recordingData = [] # Blank array, to be populated with a Recording's info
	recordingData << lineArray[0].strip # Input Recording_ID    - 0
	recordingData << lineArray[1] # Input Director        - 1
	recordingData << lineArray[3] # Input Title           - 2
	recordingData << actors # Input list of actors        - 3
	recordingData << lineArray[5] # Input Category        - 4
	recordingData << lineArray[7] # Input image file name - 5
	recordingData << lineArray[8] # Input Duration        - 6
	recordingData << lineArray[9] # Input Rating          - 7
	recordingData << lineArray[10] # Input Release Year   - 8
	recordingData << lineArray[11] # Input Price          - 9
	recordingData << lineArray[12].to_i # Input Stock Count    - 10
	videoDataArray << recordingData
} 

# Insert the actor data into MongoDB...
puts "Sending Video_Actors data to MongoDB..."
actorDataArray.each{|actor|
	insert = {"_id" => BSON::ObjectId.new, "Actor_ID" => actor[0], "Name" => actor[1], "Movies" => actor[2]}
	client[:Video_Actors].insert_one insert
	}
puts "Done!"

# Insert the actor data into MongoDB...
puts "Sending Video_Recordings data to MongoDB..."
videoDataArray.each{|recording|
	insert = {"_id" => BSON::ObjectId.new, "Recording_ID" => recording[0], "Director" => recording[1], "Title" => recording[2], "Actors" => recording[3],
			 "Category" => recording[4], "img" => recording[5], "Duration"=> recording[6], "Rating" => recording[7], "Release Year" => recording[8],
			 "Price" => recording[9], "Stock" => recording[10]}
	client[:Video_Recordings].insert_one insert
	}
puts "Done!"


# Update the DB so that the Video_Actors entries contain an array of Recordings in the format: [id_, id_, ...] with id_ being the BSON ObjectID of the recording in Video_Recordings...
puts "Updating Video_Actors Data..."

actors.find.each{|actor|
	recordings = actor.to_a[3]
	# puts recordings
	movies = []
	# puts "Actor: " + actor.to_a[2][1]
	recordings.each{|movie| # Remove the 'Movies' text from the found array...
		# puts movie
		if(movie != 'Movies')
			# Search the Video_recordings collection to get the Recording's id_ value...
			movie.each{|rec| 
				videoRecordings.find("Recording_ID" => rec).each do |entry|
					# puts "HERE!"
					movies << entry.to_a[0][1] # Insert the Recording's BSON ObjectID
				end
			}
		end
	}
	# puts "Actor: " + actor.to_a[2][1]
	# puts movies
	actors.update_one({"Actor_ID" => actor.to_a[1][1]}, '$set' => {"Movies" => movies})
}

# Update the DB so that the Video_Recordings entries contain an array of Actors in the format: [id_, id_, ...] with id_ being the BSON ObjectID of the actor in Video_Actors...
videoRecordings.find.each{|acts|
	acts = acts.to_a
	a = acts[4]
	actorsArray = []
	# puts a[1].to_s
	a[1].each{|entry|
		actor_id = entry[0]
		# puts "ID: " + actor_id.to_s
		actors.find("Actor_ID" => actor_id).each do |item|
			actorsArray << item.to_a[0][1]
		end
	}
	# puts actorsArray
	videoRecordings.update_one({"Recording_ID" => acts.to_a[1][1]}, '$set' => {"Actors" => actorsArray})
}
