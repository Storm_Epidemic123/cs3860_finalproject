# cs3860_finalProject

# Requirements:

<details><summary>This set of Ruby programs requires the following gems:</summary>
	gem install mongo
	gem install bson
	gem install table_print

</details>
	
	
When running mongoDBSetUp.rb, make sure at least two collections exist, those being named Video_Actors and Video_Recordings respectively


# Details
This program is used to convert a set of data (Video_Actors.txt and Video_Recordings.txt) into data placed inside of a MongoDB database. 
