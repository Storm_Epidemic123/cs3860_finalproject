# CS 3860 Final Project
# Joshua Rist
# Do the queries for the Lab 3 questions using my new, fancy MongoDB database :)

require('mongo')
require('bson')
require('table_print')

# run this program (LAPTOP):
# irb load 'C:\Users\ristj\Desktop\CSDatabases\cs3860_finalproject\mongoDBQueries.rb'
# run this program (DESKTOP):
# irb load 'C:\Users\Admin\Desktop\Git Repos\CS3860\cs3860_finalproject\mongoDBQueries.rb'

client = Mongo::Client.new('mongodb+srv://ristj:acpo0957@cluster0.qhsfq.mongodb.net/VideoDB?retryWrites=true&w=majority')
Mongo::Logger.logger.level = ::Logger::FATAL

actors = client[:Video_Actors]
recordings = client[:Video_Recordings]
categories = ["Action & Adventure","Comedy","Drama","Horror","Science Fiction", "Suspense"] # I don't NEED to do this, it just saves code to do this.

# puts actors.find( { "Name": 'Robert Duvall' } ).first
# puts recordings.find( { "Title": 'Top Gun' } ).first

# Prints a basic menu to the console
def printUI()
	puts "Welcome to my CS3860 Final Project: MongoDB Using Ruby!"
	puts "Please select an option:"
	puts "3 - List the number of videos for each video category"
	puts "4 - List the number of videos for each video category where the inventory is non-zero"
	puts "5 - For each actor, list the video categories that actor has appeared in"
	puts "6 - Which actors have appeared in movies in different video categories?"
	puts "7 - Which actors have not appeared in a comedy?"
	puts "8 - Which actors have appeared in both a comedy and an action adventure movie?"
	puts "exit - Exit this program"
	end

# Handles what happens when a user selects what query they would like to see

# 3 - List the number of videos for each video category
def queryThree(recordings, categories)
	puts "Query 3:"
	output = []
	# Determine what different categories we have...
	categoryTotals = []
	categories.each{|category|
		docs = recordings.find("Category" => category)
		categoryTotals << docs.count
		entry = {:category => category, :count =>(docs.count).to_s}
		output << entry
	}
	tp output # Use the 'table_print' gem to print the found data...
end

# 4 - List the number of videos for each video category where the inventory is non-zero
def queryFour(recordings)
	puts "Query 4:"
	output = []
	recordings.find("Stock" => {'$gt' => 0}).each do |recording|
		data = recording.to_a # Convert found data into an array
		entry = {:Recording => data[3][1].to_s, :Stock => data[11][1].to_s}
		# puts data[3][1].to_s + " | " + data[11][1].to_s # Print found data...
		output << entry
	end
	tp output
end

# 5 - For each actor, list the video categories that actor has appeared in
def queryFive(actors, recordings)
	puts "Query 5:"
	puts "Working..."
	output = []
	# Get all actors
	allActors = actors.find
	allActors.each{|actor|
		categories = " " # This is a string to make it easier to print later...
		movies = actor.to_a[3] # Get the movies this actor has played in... This array looks something like: [id_, ActorID, Name, Movies]
		movies.each{|movie|
			if(movie != "Movies") # Don't count the first entry in the 'Movies' array, parsing documents this way makes the 0th entry always be 'Movies' rather than anything useful...
				movie.each{|id|
					recordings.find("_id" => id).each do |recording| # Search Video_Recordings using the ObjectID ('_id') in each Actor's Movies array
					data = recording.to_a # Convert found data into an array
					if(!(categories.include? data[5][1])) # Determine if the name of the category of the found movie is already present in our categories string
						categories = categories + " " + data[5][1] # Concat. the category name to our string
						end
					end
				}
			end
		}
		entry = {:Actor => actor.to_a[2][1], :Categories => categories} # create new array in order to correctly use the table_print library
		output << entry # Add the entry to our output array
		#puts actor.to_a[2][1] + " | " + categories # Print found data...
	}
	tp output # Print the query using the table_print library
end

# 6 - Which actors have appeared in movies in different video categories?
def querySix(actors, recordings)
	puts "Query 6:"
	puts "Working..."
	output = []
	# Get all actors
	allActors = actors.find
	allActors.each{|actor|
		categories = " " # This is a string to make it easier to print later...
		same = 0
		catTotal = 0
		movies = actor.to_a[3] # Get the movies this actor has played in...
		movies.each{|movie|
			if(movie != "Movies") # Don't count the first entry in the 'Movies' array
				movie.each{|id|
					recordings.find("_id" => id).each do |recording| # Search Video_Recordings using the ObjectID ('_id') in the each Actor's Movies array
					data = recording.to_a # Convert found data into an array
					if(!(categories.include? data[5][1]))
						categories = categories + " " + data[5][1]
						catTotal = catTotal + 1
					else
						same = 1
					end
				end
				}
			end
		}
		if((same == 0) && (catTotal != 1))
			entry = {:Actor => actor.to_a[2][1], :Categories => categories}
			# puts actor.to_a[2][1] + " | " + categories # Print found data...
			output << entry
		end
		catTotal = 0
	}
	tp output
end
# 7 - Which actors have not appeared in a comedy?
def querySeven(actors, recordings)
	puts "Query 7:"
	puts "Working..."
	output = []
	# Get all actors
	allActors = actors.find
	allActors.each{|actor|
		categories = " " # This is a string to make it easier to print later...
		same = 0
		movies = actor.to_a[3] # Get the movies this actor has played in...
		movies.each{|movie|
			if(movie != "Movies") # Don't count the first entry in the 'Movies' array
				movie.each{|id|
					recordings.find("_id" => id).each do |recording| # Search Video_Recordings using the ObjectID ('_id') in the each Actor's Movies array
					data = recording.to_a # Convert found data into an array
					if(!(categories.include? data[5][1])) # If the category isn't already present in the 'categories' string we created...
						categories = categories + " " + data[5][1]
					else
						same = 1
					end
				end
				}
			end
		}
		if(!(categories.include? "Comedy"))
			entry = {:Actor => actor.to_a[2][1], :Categories => categories}
			# puts actor.to_a[2][1] + " | " + categories # Print found data...
			output << entry
		end
	}
	tp output
end
# 8 - Which actors have appeared in both a comedy and an action adventure movie?
def queryEight(actors, recordings)
	puts "Query 8:"
	puts "Working..."
	output = []
	# Get all actors
	allActors = actors.find
	allActors.each{|actor|
		categories = " " # This is a string to make it easier to print later...
		same = 0
		movies = actor.to_a[3] # Get the movies this actor has played in...
		movies.each{|movie|
			if(movie != "Movies") # Don't count the first entry in the 'Movies' array
				movie.each{|id|
					recordings.find("_id" => id).each do |recording| # Search Video_Recordings using the ObjectID ('_id') in the each Actor's Movies array
					data = recording.to_a # Convert found data into an array
					if(!(categories.include? data[5][1]))
						categories = categories + " " + data[5][1]
					else
						same = 1
					end
				end
				}
			end
		}
		if((categories.include? "Comedy") && (categories.include? "Action & Adventure"))
			# puts actor.to_a[2][1] + " | " + categories # Print found data...
			entry = {:Actor => actor.to_a[2][1], :Categories => categories}
			output << entry
		end
	}
	tp output # Print our output using the table_print gem
end


printUI() # Print our starting UI
selection = gets # Just assume valid inputs for now...
selection = selection.strip # Remove white spaces from the input
if(selection == '3')
	# run query #3
	queryThree(recordings, categories)
	end
if(selection == '4')
	# run query 4
	queryFour(recordings)
	end
if(selection == '5')
	# run query #5
	queryFive(actors, recordings)
	end
if(selection == '6')
	# run query 6
	querySix(actors, recordings)
	end
if(selection == '7')
	# run query #7
	querySeven(actors, recordings)
	end
if(selection == '8')
	# run query 8
	queryEight(actors, recordings)
	end


